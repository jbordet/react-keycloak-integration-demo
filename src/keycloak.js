import Keycloak from 'keycloak-js';

let initOptions = {
  url: 'http://localhost:8090/',
  realm: 'master',
  clientId: 'react-client',
}

const keycloak = new Keycloak(initOptions)

export const checkLogin = async (onAuthenticationCallback) => {
  try {
    const authenticated = await keycloak.init({
      onLoad: 'login-required',
      checkLoginIframe: false,
      pkceMethod: 'S256'
    });
    if(authenticated){
      onAuthenticationCallback()
    }
  } catch( err ){
    console.log(err)
  }
}

export default keycloak;