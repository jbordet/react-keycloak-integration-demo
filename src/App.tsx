import { CSSProperties } from 'react'
import './App.css'
import { Button, TextField } from '@mui/material'
import { useState } from 'react'


function App() {

  const [showNewEmployeeForm, setShowNewEmployeeForm] = useState(false)

  const rootStyle:CSSProperties = {
    marginTop: '20px',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center'
  }

  const getEmployees = () => {
    //get request to backend
  }

  return (
    <div style={rootStyle}>
      <h2 style={{fontSize: '26px'}}>Home</h2>
      
      <div style={{display: 'flex', flexDirection: 'column'}}>
        <Button style={{width: '350px'}} onClick={getEmployees} variant="contained">Get employee list</Button>
        <span>...Lista de empleados...</span>
      </div>

      <div style={{marginTop: '90px', display:'flex', flexDirection:'column', width: '350px'}}>

        <Button style={{width:'100%'}} onClick={() => setShowNewEmployeeForm(!showNewEmployeeForm)} variant="contained">Add employee</Button>
      
          {showNewEmployeeForm && 
            <div style={{display: 'flex', flexDirection:'column'}}>
              <span style={{marginTop: '20px'}}>Employee Form</span>
              <TextField style={{marginTop: '20px'}} id="employeeName" label="Name" variant="outlined" />
              <TextField style={{marginTop: '20px'}} id="idNumber" label="Id Number" variant="outlined" />
            </div>
          }
    

      </div>

    </div>
  )
}

export default App
