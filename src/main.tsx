import React, { createElement } from 'react'
import ReactDOM from 'react-dom/client'
import App from './App.tsx'
import './index.css'
import { checkLogin } from './keycloak'

const root = ReactDOM.createRoot(document.getElementById('root') as HTMLElement || document.createElement('div'))

const renderApp = () =>
  root.render(
    <React.StrictMode>
      <App />
    </React.StrictMode>
  )

checkLogin(renderApp)
